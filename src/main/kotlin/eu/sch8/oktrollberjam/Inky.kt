package eu.sch8.oktrollberjam

import processing.core.PImage

class Inky(override var board: Board, override var player: Player, override var images: Array<PImage>): Ghost() {
    override var position = PlayerPosition(14, 14)
    override var direction = 'E'

    override fun fleeMove(): Char {
        return favMove(charArrayOf('S', 'E'))
    }

}