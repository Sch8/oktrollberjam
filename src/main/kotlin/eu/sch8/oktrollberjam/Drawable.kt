package eu.sch8.oktrollberjam

import processing.core.PGraphics

interface Drawable {
    fun draw(g: PGraphics, frameCount: Int)
}