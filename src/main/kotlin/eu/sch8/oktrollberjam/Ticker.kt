package eu.sch8.oktrollberjam

class Ticker {
    private var subscribers = mutableSetOf<TickerSubscriber>()
    fun tick() {
        val now = System.currentTimeMillis()
        for (subscriber in subscribers) {
            if (subscriber.lastTick + subscriber.interval < now) {
                subscriber.lastTick = now
                subscriber.callback.invoke()
            }
        }
    }

    fun subscribe(interval: Int, callback: () -> Unit): TickerSubscriber {
        val ts = TickerSubscriber(interval, 0, callback)
        this.subscribers.add(ts)
        return ts
    }

    fun unSubscriber(tickerSubscriber: TickerSubscriber) = subscribers.remove(tickerSubscriber)
}

data class TickerSubscriber(var interval: Int, var lastTick: Long = 0, var callback: ()->Unit)
