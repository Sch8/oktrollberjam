package eu.sch8.oktrollberjam

import processing.core.PGraphics
import processing.core.PImage
import java.io.File

class Board(private val pumpkinImage: PImage) : Drawable {
    private var board: Array<CharArray>
    private val scale = 50f

    fun getBoard() = this.board

    init {
        board = readBoardFile()
    }

    private fun readBoardFile() = File("src/main/resources/board.txt").readLines().map { it.toCharArray() }.toTypedArray()

    override fun draw(g: PGraphics, frameCount: Int) {
        g.noStroke()
        for (i in board.indices)
            for (j in board[i].indices){
                g.fill(16f, 16f, 127f)
                when(board[i][j]) {
                    //wall
                    '1' -> g.rect(j*scale, i*scale+scale/2, scale/2, scale/2)
                    '2' -> g.rect(j*scale, i*scale+scale/2, scale, scale/2)
                    '3' -> g.rect(j*scale+scale/2, i*scale+scale/2, scale/2, scale/2)
                    '4' -> g.rect(j*scale, i*scale, scale/2, scale)
                    '5' -> g.rect(j*scale, i*scale, scale, scale)
                    '#' -> g.rect(j*scale, i*scale, scale, scale)//tron mode
                    '6' -> g.rect(j*scale+scale/2, i*scale, scale/2, scale)
                    '7' -> g.rect(j*scale, i*scale, scale/2, scale/2)
                    '8' -> g.rect(j*scale, i*scale, scale, scale/2)
                    '9' -> g.rect(j*scale+scale/2, i*scale, scale/2, scale/2)
                    'a' -> {g.rect(j*scale, i*scale, scale, scale);
                            g.fill(0f, 4f, 0f);
                            g.rect(j*scale+scale/2, i*scale+scale/2, scale/2+1, scale/2+1)}
                    'b' -> {g.rect(j*scale, i*scale, scale, scale);
                        g.fill(0f, 4f, 0f);
                        g.rect(j*scale-1, i*scale+scale/2, scale/2+1, scale/2+1)}
                    'c' -> {g.rect(j*scale, i*scale, scale, scale);
                        g.fill(0f, 4f, 0f);
                        g.rect(j*scale+scale/2, i*scale-1, scale/2+1, scale/2+1)}
                    'd' -> {g.rect(j*scale, i*scale, scale, scale);
                        g.fill(0f, 4f, 0f);
                        g.rect(j*scale-1, i*scale-1, scale/2+1, scale/2+1)}
                    //Possible player positions
                    'o' -> {g.image(pumpkinImage, j*scale, i*scale)}
                    '.' -> {g.fill(255f, 185f, 175f); g.ellipse(j*scale+scale/2, i*scale+scale/2, scale/2.5f, scale/2.5f)}
                    '0' -> {}
                    //center wall
                    '_' -> {if(frameCount%2==0) g.fill(16f, 16f, 127f)
                            else                g.fill(255f, 185f, 175f)
                            g.rect(j*scale, i*scale+scale/1.5f, scale, scale/4)}
                }
            }
    }

    fun cleanTron() {
        for (ca in board.indices) {
            for (c in board[ca].indices) {
                if (board[ca][c] == '#')
                    board[ca][c] = '0'
            }
        }
    }
}