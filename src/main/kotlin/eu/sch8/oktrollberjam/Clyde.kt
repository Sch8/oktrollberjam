package eu.sch8.oktrollberjam

import processing.core.PImage

class Clyde(override var board: Board, override var player: Player, override var images: Array<PImage>): Ghost() {
    override var position = PlayerPosition(12, 14)
    override var direction = 'E'

    override fun fleeMove(): Char {
        return favMove(charArrayOf('S', 'W'))
    }

}