package eu.sch8.oktrollberjam

import processing.core.PGraphics
import processing.core.PImage
import java.io.File

abstract class Ghost: Drawable {
    abstract val images: Array<PImage>
    abstract val player: Player
    abstract var position: PlayerPosition
    abstract var direction: Char
    abstract val board: Board
     private var chasingMode = false
     private var frightenedMode = false
    var scatterTimer = 0
    fun update() {
        frightenedMode = player.isRageMode()
        if (player.getPosition() == position) {
            if (frightenedMode) {
                player.add50toScore()
                position = PlayerPosition(12, 14)
                scatterTimer = 0
                Thread(
                    MusicPlayer(
                        File("src/main/resources/eat.wav"),
                        false
                    )
                ).start()
            }
            else player.die()
        }

        if (! frightenedMode && scatterTimer < 233) scatterTimer++
        chasingMode = !(scatterTimer in 0..21 || scatterTimer in 61..82 || scatterTimer in 142..157 || scatterTimer in 217..232)
        direction = when {
            frightenedMode -> fleeMove()
            chasingMode -> chaseMove()
            else -> randomMove()
        }
        when(direction) {
            'N' -> position.y--
            'S' -> position.y++
            'W' -> position.x--
            'E' -> position.x++
        }
    }

    override fun draw(g: PGraphics, imageIndex: Int) {
        when (direction) {
            'N' -> g.image(images[0 + imageIndex], position.x * 50f, position.y * 50f)
            'W' -> g.image(images[2 + imageIndex], position.x * 50f, position.y * 50f)
            'S' -> g.image(images[4 + imageIndex], position.x * 50f, position.y * 50f)
            else ->g.image(images[6 + imageIndex], position.x * 50f, position.y * 50f)
        }
    }

    abstract fun fleeMove(): Char
    private fun chaseMove(): Char {
        val possibleMoves = possibleMoves()
        val playerPosition = player.getPosition()
        return when {
            (playerPosition.x - position.x > 0 && possibleMoves.contains('E')) -> 'E'
            (playerPosition.x - position.x < 0 && possibleMoves.contains('W')) -> 'W'
            (playerPosition.y - position.y > 0 && possibleMoves.contains('S')) -> 'S'
            (playerPosition.y - position.y < 0 && possibleMoves.contains('N')) -> 'N'
            else -> randomMove()
        }
    }

    private fun possibleMoves(): CharArray {
        val possibleChars = arrayOf('o', '0', '.', '_', '#')
        val ways = mutableSetOf<Char>()
        if (possibleChars.contains(board.getBoard()[position.y-1][position.x]) && direction != 'S') ways.add('N')
        if (possibleChars.contains(board.getBoard()[position.y+1][position.x]) && direction != 'N') ways.add('S')
        if (possibleChars.contains(board.getBoard()[position.y][position.x-1]) && direction != 'E') ways.add('W')
        if (possibleChars.contains(board.getBoard()[position.y][position.x+1]) && direction != 'W') ways.add('E')
        return ways.toCharArray()
    }

    private fun randomMove() = possibleMoves().random()

    fun favMove(favOrder: CharArray): Char {
        val possibleMoves = possibleMoves()
        favOrder.forEach { if (possibleMoves.contains(it)) return it }
        return randomMove()
    }
}