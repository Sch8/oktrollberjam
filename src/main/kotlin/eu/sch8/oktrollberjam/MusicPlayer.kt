package eu.sch8.oktrollberjam

import java.io.File
import javax.sound.sampled.AudioSystem
import javax.sound.sampled.Clip

class MusicPlayer(private val file: File, private val loop: Boolean): Runnable {
    override fun run() {
        val audioIn = AudioSystem.getAudioInputStream(file)
        val clip = AudioSystem.getClip()
        clip.open(audioIn)
        if (loop) clip.loop(Clip.LOOP_CONTINUOUSLY)
        clip.start()
        while (loop or clip.isRunning) Thread.sleep(100)
    }
}