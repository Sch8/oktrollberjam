package eu.sch8.oktrollberjam

import processing.core.PGraphics
import processing.core.PImage
import java.io.File

class Player{
    private var lives = 3
    private val playerImg: Array<PImage>
    private var position = PlayerPosition(13, 23)
    private var direction: Char = 'E' //[N]orth, [E]ast, [S]outh, [W]est
    private val board: Board
    private var rageMode = 0

    //Movement Values
    private var upKey = 'w'
    private var leftKey = 'a'
    private var downKey = 's'
    private var rightKey = 'd'

    private var trollCount = 0
    private var troll = (15..45).random()

    fun getUpKey() = this.upKey
    fun getLeftKey() = this.leftKey
    fun getDownKey() = this.downKey
    fun getRightKey() = this.rightKey
    fun getLives() = this.lives
    fun isRageMode() = this.rageMode > 0
    fun getPosition() = this .position

    fun changeControll() {
        var key: Int
        do {
            key = (97..122).random()
        } while (key.toChar() == upKey || key.toChar() == leftKey || key.toChar() == downKey || key.toChar() == rightKey)
        val ran = (0..3).random()

        if (ran == 0) {
            upKey = key.toChar()
        } else if (ran == 1) {
            leftKey = key.toChar()
        } else if (ran == 2) {
            downKey = key.toChar()
        } else {
            rightKey = key.toChar()
        }
    }

    fun die() {
        if (lives <= 0) {
            position = PlayerPosition(0, 0)
            Thread(
                MusicPlayer(
                    File("src/main/resources/oh.wav"),
                    false
                )
            ).start()
        } else {
            lives--
            Thread(
                MusicPlayer(
                    File("src/main/resources/kill.wav"),
                    false
                )
            ).start()
        }
        position = PlayerPosition(13, 23)
    }

    private var score = 0
    fun getScore() = this.score
    fun add50toScore() {
        this.score += 50
    }

    private val possibleChars = arrayOf('o', '0', '.')

    constructor(board: Board, imgs: Array<PImage>) {
        this.board = board
        playerImg = imgs
    }

    fun draw(g: PGraphics, imageIndex: Int) {
        if ( lives <= 0) {}
        else {
            when (direction) {
                'N' -> g.image(playerImg[0 + imageIndex], position.x * 50f, position.y * 50f)
                'W' -> g.image(playerImg[2 + imageIndex], position.x * 50f, position.y * 50f)
                'S' -> g.image(playerImg[4 + imageIndex], position.x * 50f, position.y * 50f)
                else ->g.image(playerImg[6 + imageIndex], position.x * 50f, position.y * 50f)
            }
        }
    }

    fun update() {
        board.getBoard()[position.y][position.x] = '#'
        trollCount++
        //Perform troll
        if (trollCount % troll == 0) {
            troll = (15..45).random()
            changeControll()
        }

        if (direction == 'N') {
            if (possibleChars.contains(board.getBoard()[position.y-1][position.x]))
                position.y--
        }
        else if (direction == 'W') {
            if (possibleChars.contains(board.getBoard()[position.y][position.x-1]))
                position.x--
        }
        else if (direction == 'S') {
            if (possibleChars.contains(board.getBoard()[position.y+1][position.x]))
                position.y++
        }
        else { //direction == E
            if (possibleChars.contains(board.getBoard()[position.y][position.x+1]))
                position.x++
        }

        //Zombieman on white dot
        if (board.getBoard()[position.y][position.x] == '.') {
            board.getBoard()[position.y][position.x] = '0'
            score++
        }
        if (board.getBoard()[position.y][position.x] == 'o'){
            board.getBoard()[position.y][position.x] = '0'
            score += 10
            rageMode = 100
        }

        //Zombieman uses portal
        if (position.x == 0 && position.y == 14) {
            position.x = 27
        } else if (position.x == 27 && position.y == 14) {
            position.x = 0
        }
        if (rageMode > 0) rageMode--
    }

    fun keyPressed(key: Char) {
        if (key == upKey) {
            if (possibleChars.contains(board.getBoard()[position.y-1][position.x]))
                direction = 'N'
        }
        else if (key == leftKey) {
            if (possibleChars.contains(board.getBoard()[position.y][position.x-1]))
                direction = 'W'
        }
        else if (key == downKey) {
            if (possibleChars.contains(board.getBoard()[position.y+1][position.x]))
                direction = 'S'
        }
        else if (key == rightKey){
            if (possibleChars.contains(board.getBoard()[position.y][position.x+1]))
                direction = 'E'
        }
    }
}

data class PlayerPosition(var x:Int, var y:Int)