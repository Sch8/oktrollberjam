package eu.sch8.oktrollberjam

import processing.core.PImage

class Blinky(override var board: Board, override var player: Player, override var images: Array<PImage>): Ghost() {
    override var position = PlayerPosition(15, 14)
    override var direction = 'E'

    override fun fleeMove(): Char {
        return favMove(charArrayOf('N', 'E'))
    }

}