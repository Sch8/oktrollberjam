package eu.sch8.oktrollberjam

import processing.core.PApplet
import processing.core.PConstants
import processing.core.PImage
import java.io.File

fun main() {
    PApplet.main("eu.sch8.oktrollberjam.Main")
}

class Main: PApplet() {
    private lateinit var board: Board
    private lateinit var player: Player;
    private lateinit var playerImages: Array<PImage>
    private lateinit var pumpkinImage: PImage
    private lateinit var ghosts: Array<Ghost>
    private lateinit var blinkyImages: Array<PImage>
    private lateinit var pinkyImages: Array<PImage>
    private lateinit var inkyImages: Array<PImage>
    private lateinit var clydeImages: Array<PImage>
    private var volume = 100
    private lateinit var gameTicker: Ticker
    override fun settings() = size(1280, 720)
    private var isStartupScreen = true
    private lateinit var startupImage: Array<PImage>
    private var isCreditScreen = false
    private val musicPlayer = MusicPlayer(
        File("src/main/resources/bg.wav"),
        true
    )

    override fun setup() {
        loadImages()
        initGame()
        frameRate(5f)
        Thread(musicPlayer).start()
        super.setup()
    }

    private fun loadImages() {
        startupImage = arrayOf(loadImage("src/main/resources/startup1.png"), loadImage("src/main/resources/startup2.png"))
        pumpkinImage = loadImage("src/main/resources/pumpkin.png")
        playerImages = arrayOf(loadImage("src/main/resources/ZombiemanNorth.png"),
                loadImage("src/main/resources/Zombieman2North.png"),
                loadImage("src/main/resources/ZombiemanWest.png"),
                loadImage("src/main/resources/Zombieman2West.png"),
                loadImage("src/main/resources/ZombiemanSouth.png"),
                loadImage("src/main/resources/Zombieman2South.png"),
                loadImage("src/main/resources/ZombiemanEast.png"),
                loadImage("src/main/resources/Zombieman2East.png"))
        blinkyImages = arrayOf(loadImage("src/main/resources/ghosts/blinkyU1.png"),
                loadImage("src/main/resources/ghosts/blinkyU2.png"),
                loadImage("src/main/resources/ghosts/blinkyL1.png"),
                loadImage("src/main/resources/ghosts/blinkyL2.png"),
                loadImage("src/main/resources/ghosts/blinkyD1.png"),
                loadImage("src/main/resources/ghosts/blinkyD2.png"),
                loadImage("src/main/resources/ghosts/blinkyR1.png"),
                loadImage("src/main/resources/ghosts/blinkyR2.png"))
        pinkyImages = arrayOf(loadImage("src/main/resources/ghosts/pinkyU1.png"),
                loadImage("src/main/resources/ghosts/pinkyU2.png"),
                loadImage("src/main/resources/ghosts/pinkyL1.png"),
                loadImage("src/main/resources/ghosts/pinkyL2.png"),
                loadImage("src/main/resources/ghosts/pinkyD1.png"),
                loadImage("src/main/resources/ghosts/pinkyD2.png"),
                loadImage("src/main/resources/ghosts/pinkyR1.png"),
                loadImage("src/main/resources/ghosts/pinkyR2.png"))
        inkyImages = arrayOf(loadImage("src/main/resources/ghosts/inkyU1.png"),
                loadImage("src/main/resources/ghosts/inkyU2.png"),
                loadImage("src/main/resources/ghosts/inkyL1.png"),
                loadImage("src/main/resources/ghosts/inkyL2.png"),
                loadImage("src/main/resources/ghosts/inkyD1.png"),
                loadImage("src/main/resources/ghosts/inkyD2.png"),
                loadImage("src/main/resources/ghosts/inkyR1.png"),
                loadImage("src/main/resources/ghosts/inkyR2.png"))
        clydeImages = arrayOf(loadImage("src/main/resources/ghosts/clydeU1.png"),
                loadImage("src/main/resources/ghosts/clydeU2.png"),
                loadImage("src/main/resources/ghosts/clydeL1.png"),
                loadImage("src/main/resources/ghosts/clydeL2.png"),
                loadImage("src/main/resources/ghosts/clydeD1.png"),
                loadImage("src/main/resources/ghosts/clydeD2.png"),
                loadImage("src/main/resources/ghosts/clydeR1.png"),
                loadImage("src/main/resources/ghosts/clydeR2.png"))
    }

    private fun initGame() {
        isStartupScreen = true
        isCreditScreen = false
        board = Board(pumpkinImage)
        player = Player(board, playerImages)
        ghosts = arrayOf(
            Blinky(board, player, blinkyImages),
            Pinky(board, player, pinkyImages),
            Inky(board, player, inkyImages),
            Clyde(board, player, clydeImages)
        )
        gameTicker = Ticker()
        gameTicker.subscribe(300, player::update)
        gameTicker.subscribe(20000, board::cleanTron)
        for (ghost in ghosts) gameTicker.subscribe(300, ghost::update)
    }

    override fun draw() {
        background(0f, 4f, 0f)
        when {
            isStartupScreen -> image(startupImage[frameCount % 2], 0f, 0f, width.toFloat(), height.toFloat())
            isCreditScreen -> drawCreditScreen()
            else -> {
                tunnleBlock()
                gameTicker.tick()
                scale(.45f)
                drawGUI()
                pushMatrix()
                    translate(600f, 25f)
                    board.draw(super.g, frameCount)
                    ghosts.forEach { it.draw(g, frameCount % 2) }
                    player.draw(g, frameCount % 2)
                popMatrix()
            }
        }
    }

    private fun drawCreditScreen() {
        textAlign(LEFT)
        //Patrick de Arteaga
        fill(16f, 16f, 127f)
        textSize(80f)
        text("Credits", 100f, 100f)
        textSize(40f)
        text("Music:", 100f, 200f)
        text("Code:", 100f, 250f)
        text("Sprites:", 100f, 350f)
        fill(255f, 185f, 175f)
        text("Patrick de Arteaga", 250f, 200f)
        text("abc", 250f, 250f)
        text("Cedric Schacht", 250f, 300f)
        text("abc", 250f, 350f)

        fill(16f, 16f, 127f)
        textAlign(CENTER)
        text("Back 2 Game", (width/2).toFloat(), height-60f)
        stroke(16f, 16f, 127f)
        noFill()
        rect(width/2-125f, height-100f, 250f, 50f)
    }

    private fun drawGUI() {
        background(0f, 4f, 0f)

        pushMatrix()
        translate(140f, 100f)
        drawControl()
        popMatrix()

        pushMatrix()
        translate(140f, 2f*height-300f)
        drawScore()
        popMatrix()

        pushMatrix()
        translate(2f*width-270f, 2f*height-100f)
        drawCredits()
        popMatrix()

        pushMatrix()
        translate(2f*width-270f, 2f*height-200f)
        drawRestart()
        popMatrix()

        pushMatrix()
        translate(2f*width-370f, 200f)
        drawFakeMusicControl()
        popMatrix()

        pushMatrix()
            translate(150f, 2f*height-100f)
        drawLives()
        popMatrix()
    }

    private fun drawLives() {
        if (player.getLives() > 2) image(playerImages[2],  0f, 0f)
        if (player.getLives() > 1) image(playerImages[2], 70f, 0f)
    }

    private fun drawControl(){
        stroke(200f)
        textSize(100f)
        rect(110f,0f,100f,100f)
        rect(0f, 110f, 100f, 100f)
        rect(110f, 110f, 100f, 100f)
        rect(220f, 110f, 100f, 100f)
        fill(200f)
        textAlign(PConstants.CENTER)
        text(player.getUpKey().toUpperCase(), 160f, 90f)
        text(player.getLeftKey().toUpperCase(), 50f, 200f)
        text(player.getDownKey().toUpperCase(), 160f, 200f)
        text(player.getRightKey().toUpperCase(), 270f, 200f)
        noStroke()
    }

    private fun drawScore() {
        fill(200f)
        textAlign(LEFT, BOTTOM)
        textSize(50f)
        text("SCORE", 0f, 0f)
        textAlign(RIGHT, BOTTOM)
        text(player.getScore(), 160f, 75f)
    }

    private fun drawFakeMusicControl(){
        fill(200f)
        stroke(200f)
        rect(170f, 25f, 350f, 10f, 5f)
        stroke(0f)
        rect(170f+(volume*3.5f), 0f, 10f, 50f, 5f)
        stroke(200f)
        noFill()
        rect(0f, 0f, 145f, 50f, 5f)
        textSize(50f)
        textAlign(PConstants.CENTER)
        text("MUTE", 73f, 45f)
    }

    private fun drawCredits() {
        stroke(200f)
        noFill()
        rect(0f, 0f, 300f, 50f, 5f)
        fill(200f)
        textSize(50f)
        textAlign(CENTER)
        text("Credits", 150f, 45f)
    }

    private fun drawRestart() {
        stroke(200f)
        noFill()
        rect(0f, 0f, 300f, 50f, 5f)
        fill(200f)
        textSize(50f)
        textAlign(CENTER)
        text("Restart", 150f, 45f)
    }

    override fun keyPressed() {
        if (isStartupScreen) { isStartupScreen = false }
        else { player.keyPressed(key.toLowerCase()) }
    }

    override fun mouseClicked() {
        when {
            (mouseX in 986..1050 && mouseY in 90..115) -> Thread(
                MusicPlayer(
                    File("src/main/resources/mute.wav"),
                    false
                )
            ).start()
            (mouseX in 1060..1220 && mouseY in 90..115) -> volume = (volume + random(25f, 75f).toInt()) % 100
            (mouseX in 1030..1170 && mouseY in 600..630) -> isCreditScreen = true
            (mouseX in 1030..1170 && mouseY in 560..580) -> initGame()
            (mouseX in 520..770 && mouseY in 625..675 && isCreditScreen) -> isCreditScreen = false
            else -> println("$mouseX, $mouseY")
        }
    }

    private fun tunnleBlock() {
        if (player.getScore() in 100..180) {
            board.getBoard()[14][0] = '5'
            board.getBoard()[14][27] = '5'
        } else {
            board.getBoard()[14][0] = '#'
            board.getBoard()[14][27] = '#'
        }
    }
}